#!/bin/bash
set -e

version=4.9.0
wget https://ftp.gnu.org/gnu/screen/screen-${version}.tar.gz
tar -xf screen-${version}.tar.gz
rm -f screen-${version}.tar.gz
cd screen-${version}
./configure --prefix=$HOME/local
make
make install
cd ..
rm -rf screen-${version}

echo "defscrollback 10000" > $HOME/.screenrc
