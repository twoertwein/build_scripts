#!/bin/bash
set -e

version=6-2
wget https://www.mpi.nl/tools/elan/ELAN_${version}_linux.tar.gz
tar xf ELAN_${version}_linux.tar.gz
mkdir -p $HOME/local/bin
mv ELAN_${version}/opt/elan-${version/-/.}/ $HOME/local/
rm -f $HOME/local/bin/elan
ln -s $HOME/local/elan-${version/-/.}/bin/ELAN $HOME/local/bin/elan
rm ELAN_${version}_linux.tar.gz
