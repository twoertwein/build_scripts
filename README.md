# build scripts
This is a collection of bash scripts to locally compile some software (does not require root). The scripts will copy the compiled files to `$HOME/local`. 

Please make sure to have the following lines in your `.bashrc`:

```sh
export PATH=$HOME/local/bin:$PATH
export LIBRARY_PATH=$HOME/local/lib:$LIBRARY_PATH
export LD_LIBRARY_PATH=$HOME/local/lib:$LD_LIBRARY_PATH
export LD_RUN_PATH=$HOME/local/lib:$LD_RUN_PATH
export CPLUS_INCLUDE_PATH=$HOME/local/include:$CPLUS_INCLUDE_PATH
export C_INCLUDE_PATH=$HOME/local/include:$C_INCLUDE_PATH
export PKG_CONFIG_PATH=$HOME/local/lib/pkgconfig:$PKG_CONFIG_PATH
```
