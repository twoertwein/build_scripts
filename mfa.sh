#!/bin/bash
set -e

version=1.0.1

wget https://github.com/MontrealCorpusTools/Montreal-Forced-Aligner/releases/download/v${version}/montreal-forced-aligner_linux.tar.gz
tar -xf montreal-forced-aligner_linux.tar.gz
rm montreal-forced-aligner_linux.tar.gz
cd montreal-forced-aligner
wget -O pretrained_models/librispeech-lexicon.txt http://www.openslr.org/resources/11/librispeech-lexicon.txt
cp lib/libpython3.6m.so.1.0 lib/libpython3.6m.so
rm -rf $HOME/local/mfa
mkdir -p $HOME/local/mfa
mv * $HOME/local/mfa/
cd ..
rmdir montreal-forced-aligner
ln -s $HOME/local/mfa/bin/mfa_align $HOME/local/bin/mfa_align
ln -s $HOME/local/mfa/bin/mfa_generate_dictionary $HOME/local/bin/mfa_generate_dictionary
ln -s $HOME/local/mfa/bin/mfa_train_and_align $HOME/local/bin/mfa_train_and_align
ln -s $HOME/local/mfa/bin/mfa_train_g2p $HOME/local/bin/mfa_train_g2p

export mfa_version=${version}
